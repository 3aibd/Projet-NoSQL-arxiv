import requests
import xmltodict
from pymongo import MongoClient

try:
    print("Start querying arxiv.org")
    print("Establishing connection with mongodb client ...")
    client = MongoClient('mongodb', 27017)
    db = client['arxiv']

    if 'Article' in db.collection_names():
        print("Drop old data")
        db['Article'].drop()
    articleCollection = db['Article']

    print("Establishing connection with arxiv.org ...")
    ping = requests.get("http://export.arxiv.org/api/query")
    print("Status code:", ping.status_code)

    link = "http://export.arxiv.org/api/query?search_query=cat:{}" \
           "&start={}" \
           "&max_results={}" \
           "&sortBy=relevance" \
           "&sortOrder=descending"

    # Query params
    cat = "astro-ph.GA"
    queryStart = 0
    maxResults = 1000

    print("Query arxiv api 1000 Articles ...")
    req = requests.get(link.format(cat, queryStart, maxResults)).content.decode('utf-8')
    data = xmltodict.parse(req)["feed"]["entry"]
    print("Success !")
    print("Start inserting in mongoDB ...")
    for i, article in enumerate(data):
        articleCollection.insert_one(article)
        print("> Progress: {}/{}".format(i, maxResults), end="\r", flush=True)
    print(" o（ ^_^）o自自o（^_^ ）o ")
    client.close()
except Exception as e:
    print(e)
