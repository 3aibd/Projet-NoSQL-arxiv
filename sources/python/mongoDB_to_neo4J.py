from py2neo import Graph
from pymongo import MongoClient
import re

uri = "http://neo4j:7474"
password = "admin"

print("Establishing connection with neo4j client ...")
graph = Graph(uri, password=password)


try:
    graph.data("MATCH (n) DETACH DELETE n;")
    print("Establishing connection with mongodb client ...")
    client = MongoClient('mongodb', 27017)
    db = client['arxiv']
    articleCollection = db['Article']

    # Les articles, titres, description et nom d'affiliation peuvent avoir des caractère sépciaux qu'il faut échapper.
    # On crée un filtre qu'on appliquera à chacun de ces élements
    escape_char = str.maketrans({
        "'": r"\'",
        "\"": r"\\\"",
        "\\": r"\\\\"
    })
    objList = articleCollection.find()
    counter = 0
    print("Start pushing node ...")
    for obj in objList:    # Pour chaque entité "aricle" présente dans la base MongoDB
        # Requête de création d'un article
        article_query = "MERGE (ar:ARTICLE{id:'%s'})\n" \
                        "   ON CREATE SET ar.updated = '%s', ar.published = '%s', ar.summary = '%s', ar.title = '%s'\n" \
                        % (obj["id"], obj["updated"], obj["published"], obj["summary"].translate(escape_char), obj["title"].translate(escape_char))

        # Parcours des auteur
        authors_counter = 0     # Counter d'auteur. Nous permet de les différencier s'il y en a plusieurs
                                # Et aussi de déterminer s'il y en a

        if isinstance(obj["author"], dict):  # Si un seul auteur (dans le cas contraire, on a une variable type "list")*
            # On transforme le dict en list
            temp = obj["author"]
            obj["author"] = list()
            obj["author"].append(temp)
        for i, author_enum in enumerate(obj["author"]):  # Pour chaque auteur
            authors_counter += 1
            author_query = ""
            for k, v in author_enum.items():  # Pour chaque attribut de l'auteur
                # Si l'attribut courant est un dictionnaire (c'est le cas pour les affiliations seulement)
                if isinstance(v, dict): # S'il s'agit d'une affilaition
                    author_query += "MERGE (et:ETABLISSEMENT{name:'%s'})\n" \
                                    "   ON CREATE SET et.author_counter = 0\n" \
                                    "MERGE (au%d)-[:AFFILIATION]->(et)\n" \
                                    "   ON CREATE SET et.author_counter = et.author_counter +1\n" \
                                    % (v["#text"].translate(escape_char), authors_counter)
                elif isinstance(v, list) : # S'il s'agit d'une liste d'affiliations
                    etablissement_counter = 0
                    for aff in v :
                        etablissement_counter += 1
                        author_query += "MERGE (et%d:ETABLISSEMENT{name:'%s'})\n" \
                                        "   ON CREATE SET et%d.author_counter = 0\n" \
                                        "MERGE (au%d)-[:AFFILIATION]->(et%d)\n" \
                                        "   ON CREATE SET et%d.author_counter = et%d.author_counter + 1\n" \
                                        % (etablissement_counter,
                                           aff["#text"].translate(escape_char),
                                           etablissement_counter,
                                           authors_counter,
                                           etablissement_counter,
                                           etablissement_counter,
                                           etablissement_counter)
                else:  # Sinon (le cas du nom)
                    author_query += "MERGE (au%d:AUTHOR{name:'%s'})\n" \
                                    % (authors_counter, v.translate(escape_char))
            author_query += "MERGE (au%s)-[:CREATED]->(ar)\n" % authors_counter
            graph.data(article_query + author_query)    # Il est important d'effectué des concaténation pour obtenir
                                                        # Une seule requpete finale. Pour que les tag "ar" et "au"
                                                        # soient reconnus par Cypher

            # Remarque : La requête d'article étant un "MERGE" elle ne s'effectuera qu'une seule fois
            # Ce n'est pas grave de la mettre dans la boucle des auteurs

        # Si aucun auteur n'a été trouvé, on applique simplement la requête de création d'article
        if authors_counter == 0 :
            graph.data(article_query)
        counter += 1
        print("> Progress: {}/{}".format(counter, objList.count()), end="\r", flush=True)
    print(" 乁( ◔ ౪◔)「    ┑(￣Д ￣)┍ ")
    client.close()
except Exception as e:
    print(e)
