from py2neo import Graph

uri = "http://neo4j:7474"
password = "admin"

graph = Graph(uri, password=password)

exit = 0

while exit == 0 :
    print("[1] Statistics from an auhtor")
    print("[2] Statistiques from two authors")
    print("[3] Exit")
    choice = input("Choice : ")

    if choice == "1" :
        author_name = input("Author name : ")
        # author_name = "R. Cid Fernandes"

        article_counter_query = "MATCH (au:AUTHOR {name:'%s'})-[:CREATED]->(ar)\n" \
                                "RETURN count(ar)" \
                                % author_name
        article_counter = graph.data(article_counter_query)[0]["count(ar)"]
        print("\n%s created %d article(s)" % (author_name, article_counter))


        affiliations =  "MATCH (au:AUTHOR {name:'%s'})-[:AFFILIATION]->(et)\n" \
                        "RETURN et" \
                        % author_name
        etablissements = graph.data(affiliations)
        print("\nHe's affiliated to :")
        for et in etablissements :
            print("\t- " + et["et"]["name"])

        get_articles_query =    "MATCH (au:AUTHOR {name:'%s'})-[:CREATED]->(ar)\n" \
                                "RETURN ar.id" \
                                % author_name
        documentation = graph.data(get_articles_query)
        print("\nAll creation and participation :")
        for arcitle in documentation :
            print("\t- " + arcitle["ar.id"])

        associated_authors_query =  "MATCH (au:AUTHOR {name:'%s'})-[:CREATED]->(:ARTICLE)-[:CREATED]-(target:AUTHOR)\n" \
                                    "RETURN target\n" \
                                    % author_name
        parterns = graph.data(associated_authors_query)
        print("\nHe worked with :")
        for author in parterns :
            print("\t- " + author["target"]["name"])

    elif choice == "2" :
        author_name_1 = input("Author 1 : ")
        author_name_2 = input("Author 2 : ")
        depth = input("\nProfondeur de recherche : ")
        query_depth =   "MATCH (au:AUTHOR {name:'%s'})-[*..%s]-(n)-[*..%s]-(au2:AUTHOR{name:'%s'})\n" \
                        "RETURN n" \
                        % (author_name_1, depth, depth, author_name_2)
        print(query_depth)
        res = graph.data(query_depth)
        if isinstance(res, dict) :
            temp = res["n"]
            res["n"] = list()
            res["n"].append(temp)
        if isinstance(res, list) :
            print("Articles and affiliation :")
            for entity in res :
                if "name" in entity["n"] :
                    print(entity["n"]["name"])
                if "id" in entity["n"] :
                    print(entity["n"]["id"])
        else :
            print("Nothing in common")
    elif choice == "3" :
        exit = 1
    input("\nPress enter to continue")
    print("\n\n\n\n\n")

