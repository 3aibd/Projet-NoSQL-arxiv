# Mise en place du projet

## La structure des bases de données.

### Structure de la base MongoDB

```text
element explanation
feed elements  
│   <title> : Le titre du flux contenant une chaîne de requête canonique.
|   <id> : Un identifiant unique attribué à cette requête.
|   <updated> : La dernière fois que les résultats de recherche pour cette requête ont été mis à jour. Régler à minuit le jour en cours.
│   <link> : Une URL qui permet de récupérer ce flux via une requête GET.
|   <opensearch:totalResults> : Le nombre total de résultats de recherche pour cette requête.
|   <opensearch:startIndex> : L'index 0 du premier résultat renvoyé dans la liste des résultats totaux.
|   <opensearch:itemsPerPage> : Le nombre de résultats.
|  
└─── entry elements
│   │   <title> : Le titre de l'article.
|   |   <id> : A url http://arxiv.org/abs/id
|   |   <published> : La date à laquelle la version 1 de l'article a été soumise.
│   │   <summary> : L'article abstrait.
|   |   <author> : One for each author. Has child element <name> containing the author name.
|   |   <link> : Peut-être jusqu'à 3 URL donnée associée à cet article.
|   |   <category> : ArXiv ou ACM ou MSC catégorie pour un article si elle est présente.
|   |   <arxiv:primary_category> : La catégorie principale arXiv.
|   |   <arxiv:comment> : Les auteurs commentent le cas échéant.
|   |   <arxiv:affiliation> : L'affiliation de l'auteur inclus en tant que sous - élément <auteur> si elle est présente.
|   |   <arxiv:journal_ref> : Un journal de référence si elle est présente.
|   |   <arxiv:doi> : Une URL pour le DOI résolu à une ressource externe si elle est présente.
│
```

source : https://arxiv.org/help/api/user-manual#_details_of_atom_results_returned

## L’utilité de chaque script et leur fonctionnement.

### apiToMongoDB.py

Rôle : Recupèrer le dataset depuis http://export.arxiv.org/api/query et l'insèrer dans la base de donnée MongoDB :

* Nom de la base: arxiv
* nom de la collection : Article

### mongoDBtoNeo4J.py

Rôle : permet d'insèrer la base de donnée MongoDB dans neo4J.

* La plupart des requête d'insertion sont des `MERGE`. Ainsi, si un élément est déjà existant en base, aucun doublon ne sera crée.
* En plus des noeuds "Article" et "Author" évidents, nous avons crée le noeud "Etablissement" qui permettra de mettre en évidence les affiliations des auteurs.
* Mis à part les des articles, qui sont en réalité un url renvoyant sur l'article lui-même, seuls les noms seront stockés dans neo4j

### statistics.py

Rôle :  permet de requêter dans la base neo4J avec python.

On a préféré requêter dans la base neo4J qui correspond parfaitement à la recherche de lien entre entités.

#### Les requêtes à effectuer

* Faire des requêtes pour afficher les statistiques d’un auteur :
  * le nombre d’article publié,
  * le ou les institus dont il dépend,
  * tous les liens vers ses articles,
  * tous les auteurs avec lesquels il a collaboré.
  * affiche les éléments communs de deux auteur (si oui, un article sur lequel ils ont collaboré, si oui s’ils   travaillent dans le même institut...)

## Quickstart

### L'environnement

```shell
docker-compose build
docker-compose up -d
docker exec -it ESGI_PYTHON sh -c "cd /srv/python; sh"
```

### Lancer le job

```shell
./job.sh
```

Une présentation détaillé :
======
* le set de données.

arXiv est une archive de prépublications électroniques d'articles scientifiques dans les domaines de la physique, l'astrophysique, des mathématiques, de l'informatique, des sciences non linéaires et de la biologie quantitative, et qui est accessible gratuitement par Internet

Data set récupéré depuis le query de arxiv :
http://export.arxiv.org/api/query?search_query=all:electron&id_list=&start=0&max_results=1000


les bases de données et leur structure .
-   Structure de la base de depart : 
    - base de donnée en format xml *(voir structure ci-dessus)*
    - recupération de 1000 lignes

* les réponses aux livrables.
* d’autres choses intéressantes s’il y en a.

